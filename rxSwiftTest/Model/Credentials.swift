//
//  Credentials.swift
//  rxSwiftTest
//
//  Created by Kiril Lozovoy on 11.02.2021.
//

import Foundation

struct Credentials {
  let username: String
  let password: String
}
