//
//  LoginViewModel.swift
//  rxSwiftTest
//
//  Created by Kiril Lozovoy on 10.02.2021.
//

import Foundation
import RxSwift

struct LoginViewModel: LoginViewModelInput, LoginViewModelOutput  {
  
  //Mark: LoginViewModelInput
  var username: AnyObserver<String>
  var password: AnyObserver<String>
  var loginButtonDidTap: AnyObserver<Void>
  
  //Mark: LoginViewModelOutput
  var successObservable: Observable<Bool>
  var errorsObservable: Observable<LoginError>
  
  //Mark: Private properties
  private let usernamePublishSubject = PublishSubject<String>()
  private let passwordPublishSubject = PublishSubject<String>()
  private let loginButtonDidTapSubject = PublishSubject<Void>()
  private let successSubject = PublishSubject<Bool>()
  private let errorsSubject = PublishSubject<LoginError>()
  
  private let disposeBag = DisposeBag()
  
  private let correctUsername = "user"
  private let correctPassword = "1234"

  private var credentialsObservable: Observable<Credentials> {
    Observable.combineLatest(usernamePublishSubject.asObservable(), passwordPublishSubject.asObservable()) { (username, password) in
          Credentials(username: username, password: password)
    }
  }
  
  //Mark: Public properties
  var isValidLength: Observable<Bool> {
    Observable.combineLatest(
      usernamePublishSubject.asObservable().startWith(""),
      passwordPublishSubject.asObservable().startWith("")
    )
      .map { (username, password) in
        let pattern = "[A-Za-z0-9]{4,}"
        if let _ = username.range(of: pattern, options: .regularExpression),
           let _ = password.range(of: pattern, options: .regularExpression) {
          return true
        } else {
          return false
        }
      }
  }
  
  //Mark: Init
  init() {
    username = usernamePublishSubject.asObserver()
    password = passwordPublishSubject.asObserver()
    loginButtonDidTap = loginButtonDidTapSubject.asObserver()
    successObservable = successSubject.asObserver()
    errorsObservable = errorsSubject.asObserver()
  }
  
  func configure() {
    loginButtonDidTapSubject
      .withLatestFrom(credentialsObservable)
      .flatMapLatest { checkCredentials($0).materialize() }
      .subscribe(onNext: { event in
        switch event {
        case .next(let success):
          successSubject.onNext(success)
        case .error(let error):
          guard let error = error as? LoginError else { return }
          errorsSubject.onNext(error)
        case .completed:
          break
        }
      })
      .disposed(by: disposeBag)
  }
    
  //Mark: Private methods
  private func checkCredentials(_ credentials: Credentials) -> Observable<Bool> {
    Observable.create { observer in
      let success = credentials.username == correctUsername && credentials.password == correctPassword
      success ? observer.onNext(true) : observer.onError(LoginError.wrongCredentials)
      return Disposables.create()
    }
  }
}
