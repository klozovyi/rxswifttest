//
//  ViewModelProtocol.swift
//  rxSwiftTest
//
//  Created by Kiril Lozovoy on 11.02.2021.
//

import Foundation
import RxSwift

protocol LoginViewModelInput {
  var username: AnyObserver<String> { get }
  var password: AnyObserver<String> { get }
  var loginButtonDidTap: AnyObserver<Void> { get }
}

protocol LoginViewModelOutput {
  var successObservable: Observable<Bool> { get }
  var errorsObservable: Observable<LoginError> { get }
}
