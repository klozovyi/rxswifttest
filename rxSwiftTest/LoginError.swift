//
//  LoginError.swift
//  rxSwiftTest
//
//  Created by Kiril Lozovoy on 11.02.2021.
//

import Foundation

enum LoginError: Error {
  
  case wrongCredentials
  
  var description: String {
    switch self {
    case .wrongCredentials:
      return "Wrong login or password"
    }
  }
}
