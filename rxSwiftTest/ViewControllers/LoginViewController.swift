//
//  ViewController.swift
//  rxSwiftTest
//
//  Created by Kiril Lozovoy on 10.02.2021.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
  
  //MARK: Private properties
  private let viewModel = LoginViewModel()
  private let disposeBag = DisposeBag()
  
  //MARK: Outlets
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var loginButton: UIButton!
  
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    bindViewModel()
  }
  
  //MARK: Private methods
  private func bindViewModel() {
    viewModel.configure()
    
    usernameTextField.rx.text
      .orEmpty
      .bind(to: viewModel.username)
      .disposed(by: disposeBag)
    
    passwordTextField.rx.text
      .orEmpty
      .bind(to: viewModel.password)
      .disposed(by: disposeBag)
    
    loginButton.rx.tap
      .bind(to: viewModel.loginButtonDidTap)
      .disposed(by: disposeBag)
    
    viewModel.isValidLength
      .bind(to: loginButton.rx.isEnabled)
      .disposed(by: disposeBag)
    
    viewModel.successObservable
      .subscribe(onNext: { [unowned self] _ in
        self.showAlert(
          title: "Success",
          message: "Username and password confirmed"
          ){ _ in
            self.performSegue(withIdentifier: "toTableView", sender: self)
          }
      })
      .disposed(by: disposeBag)
    
    viewModel.errorsObservable
      .subscribe(onNext: { [unowned self] error in
        self.showAlert(
          title: "Failure",
          message: error.description
          )
      })
      .disposed(by: disposeBag)
  }
  
  private func showAlert(title: String, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
    let ac = UIAlertController(
      title: title,
      message: message,
      preferredStyle: .alert
    )
    
    let okAction = UIAlertAction(title: "OK", style: .default, handler: handler)
    ac.addAction(okAction)
    present(ac, animated: true)
  }
}

